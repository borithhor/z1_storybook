import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Cart from "./../src/components/Card";

export default {
  title: "Cart",
  component: Cart,
} as ComponentMeta<typeof Cart>;

const Template: ComponentStory<typeof Cart> = (args) => <Cart {...args} />;

export const CartComponent = Template.bind({});
CartComponent.args = {
  items: {},
};
