import type { NextPage } from "next";
import { Typography } from "@mui/material";
import styled from "styled-components";

const Home: NextPage = () => {
  return <StyledText>Hello</StyledText>;
};

const StyledText = styled(Typography)`
  font-size: 25px;
  font-weight: bold;
`;
export default Home;
