const path = require("path");
const withSvgr = require("next-plugin-svgr");
const isProd = process.env.NODE_ENV === "production";

const withTM = require("next-transpile-modules")([]);

let config = {
  swcMinify: true,
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }
    return config;
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  serverRuntimeConfig: {
    PROJECT_ROOT: __dirname,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  svgrOptions: {
    titleProp: false,
    icon: true,
  },
  experimental: {
    styledComponents: true,
    removeConsole: isProd,
  },
};
module.exports = withTM(withSvgr(config));
