## Getting Started

First, run the development server:

```bash
yarn run dev #start project
yarn run storybook #start storybook
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Open [http://localhost:6006](http://localhost:6006) with your browser to see the result.

## Learn More

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Storybook](https://storybook.js.org/) - learn about Storybook.
- [MaterialUI](https://mui.com/) - learn about Material UI.
